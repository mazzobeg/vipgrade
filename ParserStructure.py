import xml.etree.ElementTree as ET
from Node import Node

class ParserStructure :

    def __init__(self, filename) :
        self.filename = filename
    
    def getQuestions(self) :
        tree = ET.parse(self.filename)
        noeuds = {}
        noeudsXML = tree.findall(".//noeud")
        for noeudXML in noeudsXML :
            questions = {}
            questionsXML = noeudXML.findall("./question")
            for questionXML in questionsXML :
                answers = []
                answersXML = questionXML.findall("./answer")
                for answerXML in answersXML :
                    answers.append(answerXML.text)
                questions[questionXML.get('about')] = answers
            noeuds[noeudXML.get('id')] = questions
        return noeuds

    def getStates(self) :
        tree = ET.parse(self.filename)
        noeuds = {}
        noeudsXML = tree.findall(".//noeud")
        for noeudXML in noeudsXML :
            questions = {}
            questionsXML = noeudXML.findall("./question")
            for questionXML in questionsXML :
                answer = questionXML.get('answer')
                questions[questionXML.get('about')] = answer
            noeuds[noeudXML.get('id')] = questions
        return noeuds

    def getState(self, node, about):
        tree = ET.parse(self.filename)
        question = tree.find(".//noeud[@id='{}']/question[@about='{}']".format(node,about))
        return question.get('answer')

    def setStates(self, node, about, ans) :
        tree = ET.parse(self.filename)
        question = tree.find(".//noeud[@id='{}']/question[@about='{}']".format(node,about))
        question.set('answer', str(ans))
        tree.write("DAG2.xml", encoding='UTF-8', xml_declaration=True)

    def getNextQuestion(self) :
        tree = ET.parse(self.filename)
        i = 1
        while i<len(tree.findall('.//question')) and tree.find('.//question[{}]'.format(int(i))).get('answer') != 'false' :
            i+=1
        if i<len(tree.findall('.//question')) :
            about = tree.find('.//question[{}]'.format(int(i))).get('about')
            node = tree.find('.//question[{}]/..'.format(int(i))).get('id')
            return node, about
        else :
            return None, None