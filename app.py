from flask import Flask, render_template, redirect, url_for, make_response, request
import xml.etree.ElementTree as ET

from flask.helpers import url_for
from Node import Node
from ParserStructure import ParserStructure

app = Flask(__name__)

@app.route("/")
def root() :
    return redirect("/board")

@app.route("/board")
def board() :
    # securite
    ans = auth()
    if ans is False :
        return redirect(url_for('login'))
    return render_template("board.html", noeuds = 'x', questions_restantes = 'x')

@app.route("/board/structure")
def board_structure() :
    # securite
    ans = auth()
    if ans is False :
        return redirect(url_for('login'))
    # informations
    parser = ParserStructure("DAG2.xml")
    questions = parser.getQuestions()
    states = parser.getStates()
    wait = 0
    nbrQuestion = 0
    for key, values in states.items() :
        for key2, values2 in values.items() :
            nbrQuestion += 1
            if values2 == 'false' :
                wait += 1
    return render_template("structure.html", questions = questions, states = states, len = nbrQuestion, wait = wait)

@app.route("/board/structure/questions", methods=["GET"])
def board_structure_questions() :
    nId = request.args['n']
    qId = request.args['q']
    aId = request.args['a']
    if aId == 'false' :
        aId = -1
    parser = ParserStructure("DAG2.xml")
    questions = parser.getQuestions()
    answersText = questions[nId][qId]
    answers = []
    for answer in answersText :
        answers.append(answer.split(','))
    return render_template("questionStructure.html", qId=qId,  answers = answers, aId = int(aId))

@app.route("/board/structure/questions", methods=["GET", "POST"])
def board_structure_questions_reponse() :
    nId = request.args['n']
    qId = request.args['q']
    answer = request.form['radioBtn']
    aId = answer[-1]
    parser = ParserStructure("DAG2.xml")
    parser.setStates(str(nId), str(qId), str(aId))
    # renvoyer vers une nouvelle question si il en reste
    node, about = parser.getNextQuestion()
    if node is None :
        return redirect('/board/structure')
    else :
        print(node, about)
        state = parser.getState(node, about)
        return redirect(url_for('board_structure_questions', n=node , q=about, a=state))

@app.route("/board/probabilite")
def board_probabilite() : 
    pass

@app.route("/board/probabilite/questions")
def board_probabilte_questions() :
    pass

@app.route("/test")
def test() : 
    return render_template("test.html")

@app.route("/login")
def login():
    return render_template("login.html")

@app.route("/login", methods=['POST'])
def verify():
    un = request.form['un']
    mdp = request.form['mdp']
    if un == 'davidboels' and mdp == 'boelsdavid' :
        resp = make_response(redirect('/'))
        resp.set_cookie('username', 'davidboels')
        return resp
    else :
        return render_template("login.html", error=True)

def auth():
    username = request.cookies.get('username') 
    if username is None :
        return False 


if __name__ == "__main__" :
    app.run()