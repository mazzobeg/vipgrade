import xml.etree.ElementTree as ET
import itertools

class Node :

    def __init__(self, xmlFileName, id, states, type, probabilites, parents = None, parentStates = None):
        self.xmlFileName = xmlFileName
        self.id = id
        self.states = states
        self.parents = parents
        self.parentStates = parentStates
        self.type = type
        self.probabilities = probabilites

    @classmethod
    def from_xml(cls, node_as_xml, xmlFileName):
        type = node_as_xml.tag
        id = node_as_xml.attrib['id']
        statesXML = node_as_xml.findall('state')
        
        states = []
        for state in statesXML :
            states.append(state.attrib['id'])
        parentsNode = node_as_xml.find('parents')
        parentsList = None
        if parentsNode is not None :
            parentsList = parentsNode.text.split(' ')
            parentsStates = []
            for parent in parentsList :
                tree = ET.parse(xmlFileName)
                parentNode = tree.find(".//*[@id='{}']".format(parent))
                parentStatesXML = parentNode.findall('state')
                parentStates = []
                for state in parentStatesXML :
                    parentStates.append(state.attrib['id'])
                parentsStates.append(parentStates)
        if type == 'noisymax' :
            probabilites = node_as_xml.find('parameters')
        elif type == 'cpt' :
            probabilites = node_as_xml.find('probabilities')
        if parentsList is not None :
            return cls(xmlFileName, id, states, type, probabilites, parentsList, parentsStates)
        else :
            return cls(xmlFileName, id, states, type, probabilites)

    def clearProbabilites(self) :
        tree = ET.parse(self.xmlFileName)
        root = tree.getroot()
        asXmlNode = root.find("./nodes/*[@id='"+self.id+"']")
        if self.type == 'cpt' :
            probaNode = asXmlNode.find('probabilities')
            actual = probaNode.text
            actualList = actual.split(' ')
            new = '-1 ' * len(actualList)
            new = new.rstrip()
            probaNode.text = new
        tree.write(self.xmlFileName+'a', encoding="UTF-8", xml_declaration=True)

    def writeProbabilites(self, proba_as_string) : 
        tree = ET.parse(self.xmlFileName)
        root = tree.getroot()
        asXmlNode = root.find("./nodes/*[@id='"+self.id+"']")
        if self.type == 'cpt' :
            probaNode = asXmlNode.find('probabilities')
            probaNode.text = proba_as_string
        tree.write(self.xmlFileName+'a', encoding='UTF-8', xml_declaration=True)

    def display(self) :
        print("id:"+ self.id, "states:"+str(self.states), "parents:"+str(self.parents))

    def getQuestions(self) :
        questions = []
        #if marginal
        if self.parents is None :
            pq = []
            for state in self.states :
                q = { self.id : state }
                pq.append(q)
                #pq.append("Qu'elle est la probabilité que " + self.id  + " soit " + state + " ?")
            questions.append(pq)
        else : 
            if self.type != "noisymax" :
                parentsNode = []
                for parent in self.parents :
                    parentId = parent
                    tree = ET.parse('NOISYOR.xdsl')
                    root = tree.getroot()
                    parentAsXmlNode = root.find("./nodes/*[@id='"+parentId+"']")
                    parentNode = Node.from_xml(parentAsXmlNode, self.xmlFileName)
                    parentsNode.append(parentNode)
                
                parentStates = []
                for parent in parentsNode : 
                    parentStates.append(parent.states)

                combinatoryProduct = itertools.product(*parentStates)
                
                # a tester
                combinatoryProduct_asDic = []
                for product in combinatoryProduct : 
                    dict = {}
                    i = 0
                    for i in range(len(parentsNode)) :
                        dict[parentsNode[i].id] = product[i] 
                    combinatoryProduct_asDic.append(dict)
                print(combinatoryProduct_asDic)
                
                combinatoryProductAsString = []
                for product in combinatoryProduct : 
                    s = ""
                    for i in range(len(parentsNode)) :
                        if i != len(parentsNode) - 1 :
                            s += parentsNode[i].id + " est " + product[i] + ", "
                        else : 
                            s += parentsNode[i].id + " est " + product[i] + "."
                    combinatoryProductAsString.append(s)

                for productAsString in combinatoryProductAsString : 
                    pq = []
                    for state in self.states : 
                        q =  "Qu'elle est la probabilité que " + self.id  + " soit " + state  + " sachant que " + productAsString
                        pq.append(q)
                    questions.append(pq)
            else : 
                parentsNode = []
                for parent in self.parents :
                    parentId = parent
                    tree = ET.parse('NOISYOR2.xdsl')
                    root = tree.getroot()
                    parentAsXmlNode = root.find("./nodes/*[@id='"+parentId+"']")
                    parentNode = Node.from_xml(parentAsXmlNode, self.xmlFileName)
                    parentsNode.append(parentNode)

                for parent in parentsNode : 
                    
                    for i in range(len(parent.states)-1) :
                        pq = []
                        for state in self.states : 
                            q = "Qu'elle est la probabilité que " + self.id + " soit " + state + " sachant que "
                            q += parent.id + " est " + parent.states[i] + " et que le reste est absent."
                            q = {self.id : state, parent.id : parent.states[i]}
                            pq.append(q)
                        questions.append(pq)

                pq = []
                for state in self.states : 
                    q = "Qu'elle est la probabilité que " + self.id + " soit " + state + " sachant que les parents sont absents. "
                    q = {self.id : state, "MANQUANTES" : 'true'}
                    pq.append(q)
                questions.append(pq)

        return questions

    def generateQuestionAsString(self, question): 
        if self.type == 'noisymax' :
            s = "Qu'elle est la probabilité que " + list(question.keys())[0] + " soit " + list(question.values())[0]
            s +=  " sachant que " + list(question.keys())[1] + " est " + list(question.values())[1]
            s += " et les autres parents sont faux "
            s += " ?"
        
        elif self.type == 'cpt' :
            s = "Qu'elle est la probabilité que " + list(question.keys())[0]+ " soit " + list(question.values())[0]
            if len(question) > 1 :
                s +=  " sachant que " + list(question.keys())[1] + " est " + list(question.values())[1]
                for i in range(2, len(question)) :
                    s += ", " + list(question.keys())[i] + " est " + list(question.values())[i]
            s += " ?"
        return s

    def generateQuestionParentsAsString(self) :
        """[summary]

        Returns:
            [tuple]: 
        """
        if self.parents is None :
            return []
        else : 
            questions = []
            for i in range(len(self.parents)) :
                if self.parents[i] != "Gradation" :
                    q1 = "{},{},{},{}".format(self.id, self.states[0], self.parents[i], self.parentStates[i][0])
                    q2 = "{},{},{},{}".format(self.parents[i], self.parentStates[i][0], self.id, self.states[0])
                    q3 = "{},{}".format(self.id, self.parents[i])
                    pq = (q1, q2, q3)
                    questions.append(pq)
            return questions
            
                